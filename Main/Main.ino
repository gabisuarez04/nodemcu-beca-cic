#include <Rom.h>
#include <ESP8266WiFi.h>                  
#include <EEPROM.h>
#include <ESP8266WebServer.h>
#include <Web_Client.h>
#include <Modulo_Wifi.h>
#include <Web_Server.h>
#include <Data_Handler.h>

/* ---------------------- Bases de datos ----------------------- */

const int sensor_iluminacion = 0;
const int sensor_movimiento = 1;
const int sensor_obstaculo = 2; 

const int unidad_iluminacion = 0;
const int unidad_movimiento = 1;
const int unidad_obstaculo = 2;

const int nodo = 1; 
const int sector = 1; 

bool luz_estado;

/* --------------------- WIFI Access Point --------------------- */

char* red_ap = "NodeMCU-AP";    //El nombre de la red tiene que tener mas de 8 caracteres
char* clave_ap = "45678911";  //La contraseña de red tiene que tener mas de 8 caracteres     

/* ---------------------- Web Server (UI) ---------------------- */

char* usuario_web = "becacic";
char* clave_web = "esp8266";

/* ----------------------- NODEJS Server ----------------------- */

char* servidor = "142.93.244.99";
int puerto_servidor = 3000;

/* -------------------------- Logica --------------------------- */

#define pin_sensor_iluminacion A0
#define pin_sensor_movimiento D5
#define pin_sensor_obstaculo D6  
#define pin_luz D7
#define pin_luz_node D0

int tiempo_evalua_luz_solar;
int tiempo_evalua_luz_prendida;
int tiempo_evalua_presencia;

Rom *rom;
Modulo_Wifi *modulo_wifi;
Web_Server *web_server;
Web_Client *web_client;
Data_Handler *data_handler;

/* ------------------------------------------------------------- */

void setup(){

  Serial.begin(115200);
  
  rom = new Rom();
  data_handler = new Data_Handler();
  modulo_wifi = new Modulo_Wifi(red_ap, clave_ap); 
  web_server = new Web_Server(usuario_web,clave_web,80,modulo_wifi,data_handler);  
  web_client = new Web_Client(servidor, puerto_servidor);

  Neotimer* timer_iluminacion = data_handler->get_timer_luz_solar();
  timer_iluminacion->start();
 
  pinMode(pin_sensor_iluminacion, INPUT); 
  pinMode(pin_sensor_movimiento, INPUT); 
  pinMode(pin_sensor_obstaculo, INPUT);
  pinMode(pin_luz, OUTPUT);
  pinMode(pin_luz_node, OUTPUT);
    
  digitalWrite(pin_luz, HIGH); // SE APAGA LA LUZ

  digitalWrite(pin_luz_node, LOW); // SE PRENDE LA LUZ

  luz_estado = false;
  
  delay(5000);

}

bool evaluar_presencia(){
  Serial.println("Evalua presencia");
  Neotimer* timer_presencia = data_handler->get_timer_presencia();

  int valor_presencia = digitalRead(pin_sensor_movimiento);
  timer_presencia->start();
  bool done_timer_presencia = timer_presencia->done();
  Serial.println(valor_presencia);
  while ((valor_presencia != HIGH)&&(done_timer_presencia == false)) {
    valor_presencia = digitalRead(pin_sensor_movimiento);
    Serial.println(valor_presencia);
    done_timer_presencia = timer_presencia->done();
    delay(100);
    Serial.println("entro al while");   
  }

  timer_presencia->stop();
  timer_presencia->reset();

  return valor_presencia;     
}

void prender_luz(int valor_iluminacion, Neotimer* timer_artificial){
  Serial.println("PRENDER");
  digitalWrite(pin_luz, LOW); // SE PRENDE LA LUZ
  web_client->post_accion(nodo,sector,"on");
  luz_estado = true;
  timer_artificial->start();
  web_client->post_medicion(sensor_iluminacion,valor_iluminacion,unidad_iluminacion);
  data_handler->set_detecta_oscuridad(false);
}

void apagar_luz(int valor_iluminacion, Neotimer* un_timer){
  Serial.println("APAGAR");
  digitalWrite(pin_luz, HIGH); // SE APAGA LA LUZ
  web_client->post_accion(nodo,sector,"off");
  luz_estado = false;
  detener_timer(un_timer);
  web_client->post_medicion(sensor_iluminacion,valor_iluminacion,unidad_iluminacion);
}

void detener_timer(Neotimer* un_timer){
  un_timer->stop();
  un_timer->reset();  
}

void reiniciar_timer(Neotimer* un_timer){
  un_timer->stop();
  un_timer->reset();  
  un_timer->start();  
}
  
void loop(){
  
/* -------------------- Interfaz al usuario -------------------- */
  
  web_server->atender_cliente();
  ESP.wdtFeed();
/* -------------------------- Logica --------------------------- */
    
  Neotimer* timer_iluminacion = data_handler->get_timer_luz_solar();
  Neotimer* timer_artificial = data_handler->get_timer_luz_artificial();
  
  int valor_obstaculo = digitalRead(pin_sensor_obstaculo); 
  
  int valor_iluminacion = 0;
  for(int i=1; i<=10; i++){
    valor_iluminacion += analogRead(pin_sensor_iluminacion);
    valor_obstaculo = valor_obstaculo && digitalRead(pin_sensor_obstaculo); 
    delay(100);
  }
  valor_iluminacion = valor_iluminacion/10; 

  Serial.println(analogRead(pin_sensor_iluminacion));
  Serial.println(valor_obstaculo);
  if ((valor_iluminacion < (data_handler->get_umbral_solar()))&&(!luz_estado)){ 
    
    // ESTA OSCURO
    
    if (data_handler->get_detecta_oscuridad() == false){ 
            
      Serial.println("SE DETECTO OSCURIDAD POR PRIMERA VEZ");
      
      data_handler->set_detecta_oscuridad(true);    
           
      if (evaluar_presencia()){
        prender_luz(valor_iluminacion, timer_artificial);
      }
    
    } else {
      
      if (valor_obstaculo == 1){
        if (evaluar_presencia()){
          prender_luz(valor_iluminacion, timer_artificial);
        }  
      }
    } 
  }
   
  if (luz_estado){
    
    if (timer_artificial->done()){
        if (!evaluar_presencia()){
          apagar_luz(valor_iluminacion, timer_artificial);
        }
        else{
          reiniciar_timer(timer_artificial);
        }        
    }     
  }

  if (timer_iluminacion->done()){
    web_client->post_medicion(sensor_iluminacion,valor_iluminacion,unidad_iluminacion);    
    reiniciar_timer(timer_iluminacion);      
  }

}


