Main.ino es el programa principal que invoca a las librerías que se encuentran dentro del directorio /libraries:

- Rom.h: se encarga del almacenamiento y recuperación de la información en la ROM. En la memoria ROM se almacenan los datos de los umbrales de iluminación, 
de la red Wifi a la cual se conecta el NodeMCU, y de los timers que se utilizan en la lógica de gestión de la iluminación 
- Web_Client.h: se comunica con el servicio web al que le envía los datos medidos por los sensores y las acciones determinadas por el microcontrolador 
para que almacene esta información en la base de datos
- Modulo_Wifi.h: se encarga de la conectividad Wifi, configurando al NodeMCU como punto de acceso y estación
- Web_Server.h: levanta un servidor web con interfaz gráfica para poder configurar los datos que luego se almacenan en la ROM
- Data_Handler.h: se encarga de gestionar los estados de iluminación hallados durante la ejecución del sistema embebido, como por ejemplo si es la primera 
vez que se detecta oscuridad o no. 
- Neotimer: es una librería propia de Arduino que se utiliza para gestionar timers.

Los umbrales de iluminación, la red Wifi a la cual se conecta el NodeMCU, y los timers pueden ser moficados mediante
la interfaz gráfica que provee el servidor web. Para que una persona pueda acceder al servidor web debe entrar 
a la red wifi y acceder al mismo mediante un navegador web. Para ello se necesita utilizar la siguiente información:

Red Wifi NodeMCU: "NodeMCU-AP" 
Contraseña: "45678911"

Servidor web: 192.168.29.1
Usuario: "becacic";
Contraseña: "esp8266";

Los datos previamente mencionados se encuentran configurados en el archivo Main.ino y Modulo_Wifi.cpp. 

