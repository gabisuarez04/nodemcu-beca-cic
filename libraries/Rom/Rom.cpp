#include "Arduino.h"
#include "Rom.h"
#include <EEPROM.h>

Rom::Rom()
{
  // carga el contenido (512 bytes) de flash en una memoria caché de 512 bytes en la memoria RAM
  EEPROM.begin(512);
     
  direccion_wifi = 0;
  direccion_iluminacion = 64;
  direccion_timers = 128;
  direccion_luz = 192; 
}

void Rom::set_wifi(String una_red, String una_clave)
{
    Datos_Wifi datos;
    char red[32];
    una_red.toCharArray(red, 32) ;
    char clave[32];
    una_clave.toCharArray(clave, 32);
    strcpy(datos.red_wifi,red);
    strcpy(datos.clave_wifi,clave);
    EEPROM.put(direccion_wifi,datos);
    EEPROM.commit(); 
    
}

void Rom::set_iluminacion(String umbral_solar, String umbral_artificial)
{
    Datos_Iluminacion datos;

    datos.umbral_solar = umbral_solar.toInt();
    datos.umbral_artificial = umbral_artificial.toInt();

    EEPROM.put(direccion_iluminacion,datos);
    EEPROM.commit(); 

}

void Rom::set_timers(String timer_solar, String timer_artificial, String timer_presencia)
{
    Datos_Timers datos;

    datos.timer_solar = timer_solar.toInt();
    datos.timer_artificial = timer_artificial.toInt();
    datos.timer_presencia = timer_presencia.toInt();

    EEPROM.put(direccion_timers,datos);
    EEPROM.commit();  
}

void Rom::set_prendido(bool un_boolean)
{
    Accion_Luz datos;

    datos.prendio = un_boolean;

    EEPROM.put(direccion_luz,datos);
    EEPROM.commit();  
}

bool Rom::get_prendido()
{
    Accion_Luz datos;
    EEPROM.get(direccion_luz,datos);
    return datos.prendio; 
}

Datos_Wifi Rom::get_wifi()
{
    Datos_Wifi datos;
    EEPROM.get(direccion_wifi,datos);
    return datos; 
}

Datos_Iluminacion Rom::get_iluminacion()
{
    Datos_Iluminacion datos;
    EEPROM.get(direccion_iluminacion,datos);
    return datos; 
}  

Datos_Timers Rom::get_timers()
{
    Datos_Timers datos;
    EEPROM.get(direccion_timers,datos);
    return datos; 
}  