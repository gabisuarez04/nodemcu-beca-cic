#ifndef Rom_h
#define Rom_h

#include "Arduino.h"
#include <EEPROM.h>

struct Datos_Wifi{ 
    char red_wifi[32];
    char clave_wifi[32];
};
struct Datos_Iluminacion{ 
    int umbral_solar;
    int umbral_artificial;
};
struct Datos_Timers{ 
    int timer_solar;
    int timer_artificial;
    int timer_presencia;
};

struct Accion_Luz{ 
    bool prendio;
};

class Rom
{
  public:
    Rom();
    void set_wifi(String una_red, String una_clave);
    void set_iluminacion(String umbral_solar, String umbral_artificial);
    void set_timers(String timer_solar, String timer_artificial, String timer_presencia);
    void set_prendido(bool un_boolean);
    Datos_Wifi get_wifi();
    Datos_Iluminacion get_iluminacion();
    Datos_Timers get_timers();
    bool get_prendido();
  private:
    uint direccion_wifi;
    uint direccion_iluminacion;
    uint direccion_timers;
    uint direccion_luz;  
};

#endif