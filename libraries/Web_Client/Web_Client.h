#ifndef Web_Client_h
#define Web_Client_h

#include "Arduino.h"
#include <ESP8266WiFi.h>

class Web_Client
{
  public:
    Web_Client(char* un_servidor, int un_puerto);
    void enviar_request(String peticion);
    void post_medicion(int sensor, int medicion, int unidad);
    void post_accion(int nodo, int sector, String accion);
    void post_estado(int nodo, int sector, String estado);
  private:
    char* servidor;
    int puerto_servidor;
};

#endif
