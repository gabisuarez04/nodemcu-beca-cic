#include "Arduino.h"
#include "Web_Client.h"

Web_Client::Web_Client(char* un_servidor, int un_puerto)
{
  servidor = un_servidor;
  puerto_servidor = un_puerto;
}

void Web_Client::enviar_request(String peticion)
{
  if (!WiFi.isConnected()){
    Serial.println("Error al enviar mensaje");
    Serial.println("Sin conexion a red Wifi");
    return;
  }

  WiFiClient client;
  Serial.println("########## SERVER ###########");
  Serial.println(servidor);
  int reintentos = 3;

  while ((reintentos > 0) && (!client.connect(servidor, puerto_servidor))){
    Serial.println("Ha fallado la conexión");
    reintentos-=1;
  } 
  if (reintentos == 0){
    Serial.println("ERROR al conectarse al servidor Node");
    return;
  }

  reintentos = 3;
  bool terminar_while = false;
  while ((reintentos > 0)&&(terminar_while == false)){
    client.print(peticion);
    unsigned long timeout = millis();
    while (client.available() == 0) {
      if (millis() - timeout > 5000) {
        Serial.println("TIMEOUT");
        reintentos-=1;
        break;
      }
    }
    if (client.available()){
      terminar_while = true;
    }
  }
  
  if (terminar_while == false){
    Serial.println("TIMEOUT ENVIO REQUEST");
    client.stop();
    return;
  } 

  // Consulta la memoria libre
  Serial.printf("\nMemoria libre en el ESP8266: %d Bytes\n\n",ESP.getFreeHeap());

  // Imprime la respuesta
  while(client.available()){
    String line = client.readStringUntil('\r');
    Serial.print(line);
  }
  
  Serial.println();
  Serial.println("Cerrando la conexión");
}

void Web_Client::post_medicion(int sensor, int medicion, int unidad)
{
  String path = "/mediciones";
  String valores = "/"+String(sensor)+"&"+String(medicion)+"&"+String(unidad);
  String url = path+valores;
  enviar_request(String("POST ") + url + " HTTP/1.1\r\n" +
         "Host: " + servidor + "\r\n" +
         "Connection: close\r\n\r\n");
  Serial.println("SE EJECUTO POST MEDICION");
}

void Web_Client::post_accion(int nodo, int sector, String accion)
{
  String path = "/acciones";
  String valores = "/"+String(nodo)+"&"+String(sector)+"&"+accion;
  String url = path+valores;
  enviar_request(String("POST ") + url + " HTTP/1.1\r\n" +
         "Host: " + servidor + "\r\n" +
         "Connection: close\r\n\r\n");
  Serial.println("SE EJECUTO POST ACCION");
}

void Web_Client::post_estado(int nodo, int sector, String estado)
{
  String path = "/estados";
  String valores = "/"+String(nodo)+"&"+String(sector)+"&"+estado;
  String url = path+valores;
  enviar_request(String("POST ") + url + " HTTP/1.1\r\n" +
         "Host: " + servidor + "\r\n" +
         "Connection: close\r\n\r\n");
  Serial.println("SE EJECUTO POST ACCION");
}