#ifndef Web_Server_h
#define Web_Server_h

#include "Arduino.h"
#include <ESP8266WebServer.h>
#include <Rom.h> 
#include "Modulo_Wifi.h"
#include "Data_Handler.h"

class Web_Server
{
  public:
    
    Web_Server(char* un_usuario, char* una_clave, int un_puerto, Modulo_Wifi* un_modulo, Data_Handler* un_handler);
    void logout(); 
    void handlers();
    void interfaz_menu();
    void interfaz_wifi();
    void controlador_wifi();
    void controlador_iluminacion();
    void interfaz_iluminacion();
    void controlador_timers();
    void atender_cliente();
    void redirigir(String path);
    void interfaz_timers();
    char* get_usuario();
    char* get_clave();
    void set_usuario(char* un_usuario);
    void set_clave(char* una_clave);
  private:
    char* usuario;
    char* clave;
    int puerto;
    Rom *rom;
    ESP8266WebServer server;  
    Modulo_Wifi* modulo_wifi; 
    Data_Handler *data_handler;
};

#endif
