#include "Arduino.h"
#include "Web_Server.h"
#include "Web_Client.h"

Web_Server::Web_Server(char* un_usuario,char* una_clave, int un_puerto, Modulo_Wifi* un_modulo, Data_Handler* un_handler)
{
  ESP8266WebServer server(un_puerto); 
  set_usuario(un_usuario);
  set_clave(una_clave);
  rom = new Rom();
  handlers(); 
  modulo_wifi = un_modulo;
  data_handler = un_handler;
}

char* Web_Server::get_usuario(){
  return usuario;  
}

char* Web_Server::get_clave(){
  return clave;  
}

void Web_Server::handlers(){

  server.on("/", [this]() {
    if (!server.authenticate(usuario, clave)) {
      server.requestAuthentication();
      return;
    }
    redirigir("/menu");
  });  
  server.on("/menu", [this](){
    interfaz_menu();
  });
  server.on("/timers", [this](){
    interfaz_timers();
  });
  server.on("/timers/guardar", [this](){
    controlador_timers();
  });
  server.on("/iluminacion", [this](){
    interfaz_iluminacion();
  });
  server.on("/iluminacion/guardar", [this](){
    controlador_iluminacion();
  });
  server.on("/wifi", [this](){
    interfaz_wifi();
  });
  server.on("/wifi/guardar", [this](){
    controlador_wifi();
  });
  server.on("/logout", [this](){
    logout();
  });

  server.begin();

}

void Web_Server::redirigir(String path){
  server.sendHeader("Location", path);
  server.sendHeader("Cache-Control", "no-cache");
  server.send(302);  

}

void Web_Server::set_usuario(char* un_usuario)
{
  usuario = un_usuario;
}

void Web_Server::set_clave(char* una_clave)
{
  clave = una_clave;
}

void Web_Server::interfaz_menu(){

  char htmlResponse[1000];

  if (!server.authenticate(usuario, clave)) {
      return redirigir("/");        
  } 
  
  snprintf ( htmlResponse, 1000,
"<!DOCTYPE html>\
<html lang=\"en\">\
  <head>\
    <meta charset=\"utf-8\">\
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\
  </head>\
  <body>\
          <h1>Configuracion</h1>\
          <p>Configurar red de Wifi:<a href='/wifi'><br>Red Wifi</a></p><p>Configurar parametros de iluminacion:<a href='/iluminacion'><br>Iluminacion</a></p>\
          <p>Configurar timers:<a href='/timers'><br>Timers</a></p>\  
          <br><br><a href='/logout'>Logout</a>\
  </body>\
</html>"); 

  server.send ( 200, "text/html", htmlResponse );  

}

void Web_Server::controlador_timers(){

  if (!server.authenticate(usuario, clave)) {
      return redirigir("/");        
  }
  
  rom->set_timers(server.arg("timer_solar"),server.arg("timer_artificial"),server.arg("timer_presencia"));
  redirigir("/timers"); 
  delay(1000);
  ESP.restart();

}

void Web_Server::interfaz_timers(){

  char htmlResponse[1500];

  if (!server.authenticate(usuario, clave)) {
      return redirigir("/");        
  }
  
  Datos_Timers datos = rom->get_timers();
    
  snprintf ( htmlResponse, 1500,
"<!DOCTYPE html>\
<html lang=\"en\">\
  <head>\
    <meta charset=\"utf-8\">\
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\
  </head>\
  <body>\
          <h1>Configuracion de Timers</h1>\
          <form action='/timers/guardar' style='width:210px'>\
          Tiempo de espera nueva medicion (ms):\ 
          <br><input type='text' name='timer_solar' id='timer_solar' size=25 autofocus value='%d' style='padding:3px 0px; border-radius:5px;'> \
          <br><br>Tiempo de espera de luz artificial (ms):\ 
          <br><input type='text' name='timer_artificial' id='timer_artificial' size=25 autofocus value='%d' style='padding:3px 0px; border-radius:5px;'> \ 
          <br><br>Tiempo deteccion presencia (ms):\ 
          <br><input type='text' name='timer_presencia' id='timer_presencia' size=25 autofocus value='%d' style='padding:3px 0px; border-radius:5px;'> \ 
          <br><br><a href='/menu'>Volver</a><input type='submit' value='Aceptar' style='margin:auto; background-color:green; color:#A9F5A9; padding:10px; border:2px; border-radius:5px; float:right;'>\
          </form>\
  </body>\
</html>", datos.timer_solar, datos.timer_artificial, datos.timer_presencia); 

   server.send ( 200, "text/html", htmlResponse );  

}

void Web_Server::controlador_iluminacion(){

  if (!server.authenticate(usuario, clave)) {
      return redirigir("/");        
  }

  rom->set_iluminacion(server.arg("umbral_solar"), server.arg("umbral_artificial"));
  data_handler->set_iluminacion();

  redirigir("/iluminacion");
}

void Web_Server::interfaz_iluminacion(){

  char htmlResponse[1000];

  if (!server.authenticate(usuario, clave)) {
      return redirigir("/");        
  }

  Datos_Iluminacion datos = rom->get_iluminacion();
    
  snprintf ( htmlResponse, 1000,
"<!DOCTYPE html>\
<html lang=\"en\">\
  <head>\
    <meta charset=\"utf-8\">\
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\
  </head>\
  <body>\
          <h1>Parametros de Iluminacion</h1>\
          <form action='/iluminacion/guardar' style='width:210px'>\
          Umbral solar:\ 
          <br><input type='text' name='umbral_solar' id='umbral_solar' size=25 autofocus value='%d' style='padding:3px 0px; border-radius:5px;'> \
          <br><br>Umbral artificial:\ 
          <br><input type='text' name='umbral_artificial' id='umbral_artificial' size=25 autofocus value='%d' style='padding:3px 0px; border-radius:5px;'> \ 
          <br><br><a href='/menu'>Volver</a><input type='submit' value='Aceptar' style='margin:auto; background-color:green; color:#A9F5A9; padding:10px; border:2px; border-radius:5px; float:right;'>\
          </form>\
  </body>\
</html>", datos.umbral_solar, datos.umbral_artificial); 

   server.send ( 200, "text/html", htmlResponse );  

}

void Web_Server::controlador_wifi(){

  if (!server.authenticate(usuario, clave)) {
      return redirigir("/");        
  }
  rom->set_wifi(server.arg("red_wifi"), server.arg("clave_wifi"));
  delay(1000);
  
  redirigir("/wifi");
  delay(1000);
  ESP.restart();
}

void Web_Server::interfaz_wifi(){

  char htmlResponse[1000];

  if (!server.authenticate(usuario, clave)) {
      return redirigir("/");        
  }
  
  Datos_Wifi datos = rom->get_wifi();
      
  snprintf ( htmlResponse, 1000,
"<!DOCTYPE html>\
<html lang=\"en\">\
  <head>\
    <meta charset=\"utf-8\">\
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\
  </head>\
  <body>\
          <h1>Red Wifi</h1>\
          <form action='/wifi/guardar' style='width:210px'>\
          Red Wifi:\ 
          <br><input type='text' name='red_wifi' id='red_wifi' size=25 autofocus value='%s' style='padding:3px 0px; border-radius:5px;'> \
          <br><br>Clave:\ 
          <br><input type='text' name='clave_wifi' id='clave_wifi' size=25 autofocus value='%s' style='padding:3px 0px; border-radius:5px;'> \ 
          <br><br><a href='/menu'>Volver</a><input type='submit' value='Aceptar' style='margin:auto; background-color:green; color:#A9F5A9; padding:10px; border:2px; border-radius:5px; float:right;'>\
          </form>\
  </body>\
</html>", datos.red_wifi, datos.clave_wifi); 

   server.send ( 200, "text/html", htmlResponse );  
}

void Web_Server::logout(){

  char htmlResponse[1000];

  snprintf ( htmlResponse, 1000,
"<!DOCTYPE html>\
<html lang=\"en\">\
  <head>\
    <meta charset=\"utf-8\">\
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\
    <meta http-equiv=\"refresh\" content=\"0; url=/\" />\
  </head>\
  <body>\
  </body>\
</html>"); 

  server.send(401, "text/html", htmlResponse);          

}

void Web_Server::atender_cliente()
{
    server.handleClient();
}

