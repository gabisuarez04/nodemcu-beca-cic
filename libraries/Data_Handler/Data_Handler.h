#ifndef Data_Handler_h
#define Data_Handler_h

#include "Arduino.h"
#include "Rom.h"
#include <neotimer.h>

class Data_Handler
{
  public:
    Data_Handler();
    void init_semaforos();
    void init_timers();
    Neotimer* get_timer_luz_solar();
    Neotimer* get_timer_luz_artificial();
    Neotimer* get_timer_presencia();
    void set_iluminacion();
    int get_umbral_solar();
    int get_umbral_artificial();
    bool get_detecta_luz_solar();
    void set_detecta_luz_solar(bool un_boolean);
    bool get_detecta_luz_prendida();
    void set_detecta_luz_prendida(bool un_boolean);
    bool get_detecta_oscuridad();
    void set_detecta_oscuridad(bool un_boolean);
  private:
    int umbral_solar;
    int umbral_artificial;
    Neotimer *timer_luz_solar;
    Neotimer *timer_luz_artificial;
    Neotimer *timer_presencia;
    Rom *rom;
    bool detecta_luz_solar;
    bool detecta_luz_prendida;
    bool detecta_oscuridad;

};

#endif
