#include "Arduino.h"
#include "Data_Handler.h"

Data_Handler::Data_Handler()
{
  rom = new Rom();
  init_semaforos();
  init_timers();
  set_iluminacion();
}

void Data_Handler::init_semaforos()
{
  detecta_luz_solar = false;
  detecta_luz_prendida = false;
  detecta_oscuridad = false;
}

void Data_Handler::init_timers()
{
  Datos_Timers datos = rom->get_timers();

  timer_luz_solar = new Neotimer(datos.timer_solar);
  timer_luz_artificial = new Neotimer(datos.timer_artificial);
  timer_presencia = new Neotimer(datos.timer_presencia);
}

Neotimer* Data_Handler::get_timer_luz_solar()
{
  return timer_luz_solar;
}

Neotimer* Data_Handler::get_timer_luz_artificial()
{
  return timer_luz_artificial;
}

Neotimer* Data_Handler::get_timer_presencia()
{
  return timer_presencia;
}

void Data_Handler::set_iluminacion()
{
  Datos_Iluminacion iluminacion = rom->get_iluminacion();

  umbral_solar = iluminacion.umbral_solar;
  umbral_artificial = iluminacion.umbral_artificial;
}

int Data_Handler::get_umbral_solar()
{
  return umbral_solar;
}

int Data_Handler::get_umbral_artificial()
{
  return umbral_artificial;
}

bool Data_Handler::get_detecta_luz_solar()
{
  return detecta_luz_solar;
}

void Data_Handler::set_detecta_luz_solar(bool un_boolean)
{
  detecta_luz_solar = un_boolean;
}

bool Data_Handler::get_detecta_luz_prendida()
{
  return detecta_luz_prendida;
}

void Data_Handler::set_detecta_luz_prendida(bool un_boolean)
{
  detecta_luz_prendida = un_boolean;
}

bool Data_Handler::get_detecta_oscuridad()
{
  return detecta_oscuridad;
}

void Data_Handler::set_detecta_oscuridad(bool un_boolean)
{
  detecta_oscuridad = un_boolean;
}