#ifndef Modulo_Wifi_h
#define Modulo_Wifi_h

#include "Arduino.h"
#include <ESP8266WiFi.h>
#include <Rom.h>
#include "Web_Client.h"


class Modulo_Wifi
{
  public:
    Modulo_Wifi(char* una_red, char* una_clave);
    void update_st();
    void set_red_st(char* una_red);
    char* get_red_st();
  private:
    char* red_ap;
    char* clave_ap;
    char* red_st;
    char* clave_st;
    Rom *rom;
};

#endif
