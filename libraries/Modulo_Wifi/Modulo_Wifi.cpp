#include "Arduino.h"
#include "Modulo_Wifi.h"

Modulo_Wifi::Modulo_Wifi(char* una_red, char* una_clave)
{
  red_ap = una_red;
  clave_ap = una_clave;
  rom = new Rom();

  WiFi.mode(WIFI_AP_STA);

  IPAddress local_ip(192, 168, 29, 1);
  IPAddress gateway(192, 168, 29, 254);
  IPAddress subnet(255, 255, 255, 0);
  //WiFi.config(local_ip, gateway, subnet);

  WiFi.softAPConfig(local_ip, gateway, subnet);
  WiFi.softAP(red_ap, clave_ap, 6, 0); //Red con clave, en el canal 1 y visible
  WiFi.setAutoReconnect(false); // Para evitar que se vuelva a conectar si la funcion begin del Station falla
  
  red_st = (char*) malloc (32);
  clave_st = (char*) malloc (32);
  
  update_st();
  Serial.println("################# Constructor ##############");
  Serial.println(red_st);
  Serial.println(clave_st);
    
  WiFi.begin(red_st, clave_st);
  
  delay(3000);

  Serial.println();
  Serial.print("Direccion IP Access Point:  ");     //Imprime la dirección IP
  Serial.println(WiFi.softAPIP()); 
  Serial.print("Direccion MAC Access Point: ");     //Imprime la dirección MAC
  Serial.println(WiFi.softAPmacAddress()); 
  Serial.print("Direccion IP dentro de red de Wifi: ");  
  Serial.println(WiFi.localIP());
  Serial.print("Esta conectado a un AP: ");
  Serial.println(WiFi.isConnected()); 
  
}

void Modulo_Wifi::update_st()
{
  Datos_Wifi datos_rom = rom->get_wifi();  

  strcpy (red_st,datos_rom.red_wifi); 
  strcpy (clave_st,datos_rom.clave_wifi);
}

